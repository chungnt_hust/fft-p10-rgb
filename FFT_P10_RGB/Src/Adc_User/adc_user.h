/*
 * adc_user.h
 *
 *  Created on: Mar 6, 2020
 *      Author: chungnguyen
 */
 
#ifndef _ADC_USER_H_
#define _ADC_USER_H_
#include "stm32f3xx_hal.h"
#include "adc.h"

void ADC_User_calib(void);
int16_t ADC_User_read(void);

#endif // _ADC_USER_H_
