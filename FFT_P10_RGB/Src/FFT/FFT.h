/*
 */
 
#ifndef _FFT_H_
#define _FFT_H_

#include "stm32f3xx_hal.h"
#include "defValue.h"

#define FFT_DEFAULT_SAMPLING_FREQ 0
extern const uint16_t bitrev_table_ui16[];
extern const int16_t twiddleTableCos[];
extern const int16_t twiddleTableSin[];

void FFT_initFreqSampling(uint8_t freq);
void FFT_calculate(int16_t *R);
void FFT_TimerInteruptHandle(TIM_HandleTypeDef *htim);
void FFT_Process(void);
#endif /**/
