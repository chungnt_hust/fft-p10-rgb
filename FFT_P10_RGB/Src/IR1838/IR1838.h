/*
 * IrDecode.h
 *
 *  Created on: Feb 23, 2019
 *      Author: chungnguyen
 */

#ifndef IR1838_H_
#define IR1838_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>
typedef struct
{
	uint32_t tempCount1;
	uint32_t tempCount2;
	uint32_t value[40];
	uint8_t index;
	uint8_t timeOut;
} Ir_Type_t;

/* Init interrupt input capture */
void IR1838_Init(void);
/* Handle callback capture */
void IR1838_HandleEvent(void);
/* Process new code */
void IR1838_Process(void);
#endif /* IR1838_H_ */
