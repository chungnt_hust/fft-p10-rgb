/*
 * uart_user.h
 *
 *  Created on: Jan 30, 2019
 *      Author: chungnguyen
 */

#ifndef UART_USER_H_
#define UART_USER_H_
#include "myHeader.h"

#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;

void Uart_Init(void);
void Uart_Process(void);
void Uart_ProcessNewData(void);
#endif /* UART_USER_H_ */
