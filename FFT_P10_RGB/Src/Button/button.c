/*
 * keyboard.c
 *
 *  Created on: Mar 3, 2019
 *      Author: chungnguyen
 */
#include "button.h"
#include "myHeader.h"
/* */
#define TIME_DEBOUNCE 20
#define TIME_HOLDING1 3000
#define TIME_HOLDING2 5000
#define TIME_HOLDING_TIMEOUT 10000

typedef enum
{
   BTN_ACTIVE = 0,
   BTN_INACTIVE = 1,
} btn_status_t;

typedef enum
{
	BTN_1 = 0,
	BTN_2,
	BTN_3,
	BTN_MAX_PIN
} Pin_Btn;

typedef enum
{
	BTN_STATE_RELEASE = 0,
	BTN_STATE_DEBOUNCE,
	BTN_STATE_PRESS,
	BTN_STATE_HOLD1,
	BTN_STATE_HOLD2,
} State_btn;

static struct
{
    uint8_t btnActive;
    uint8_t state;
    uint32_t timeStartPress;
    uint32_t timeTotalPress;
} Control_btn[BTN_MAX_PIN];
static GPIO_TypeDef* gpioPort[] = {GPIOB, GPIOB, GPIOB};
static uint16_t gpioPin[] = {GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15};

void Btn_Process(void)
{
	for(uint8_t i = 0; i < BTN_MAX_PIN; i++)
	{
		Control_btn[i].btnActive = HAL_GPIO_ReadPin(gpioPort[i], gpioPin[i]);
		Control_btn[i].timeTotalPress = Timer_elapsedtime(g_Systime, Control_btn[i].timeStartPress);
		switch(Control_btn[i].state)
		{
			case BTN_STATE_RELEASE:
				if(Control_btn[i].btnActive == BTN_ACTIVE)
				{
					Control_btn[i].timeStartPress = g_Systime;
					Control_btn[i].state = BTN_STATE_DEBOUNCE;
				} break;
			case BTN_STATE_DEBOUNCE:
                if(Control_btn[i].timeTotalPress > TIME_DEBOUNCE)
                {
                	Control_btn[i].state = BTN_STATE_PRESS;
                } break;
			case BTN_STATE_PRESS:
                if(Control_btn[i].timeTotalPress > TIME_HOLDING1)
                {
                	Control_btn[i].state = BTN_STATE_HOLD1;
                    /*-------------------------------*/
                    /*   call hoding1 event btn */
                    Btn_holdEvent(i, TIME_HOLDING1);
                } break;
			case BTN_STATE_HOLD1:
                if(Control_btn[i].timeTotalPress > TIME_HOLDING2)
                {
                	Control_btn[i].state = BTN_STATE_HOLD2;
                    /*-------------------------------*/
                    /*   call hoding1 event btn */
                    Btn_holdEvent(i, TIME_HOLDING2);
                } break;
		}

		if(Control_btn[i].state != BTN_STATE_RELEASE && Control_btn[i].state != BTN_STATE_DEBOUNCE)
		{
			if(Control_btn[i].btnActive == BTN_INACTIVE)
			{
				Control_btn[i].state = BTN_STATE_RELEASE;
				/*-------------------------------*/
				/*   call release event btn */
				Btn_releaseEvent(i, Control_btn[i].timeTotalPress);
			}
		}
	}
}

void Btn_releaseEvent(uint8_t btnNum, uint32_t timeHolding)
{
	printf("BTN: release event btnNum[%d], timePress[%d]\n", (int)btnNum, (int)timeHolding);
}

void Btn_holdEvent(uint8_t btnNum, uint32_t timeHolding)
{
	printf("BTN: hold event btnNum[%d], timePress[%d]\n", (int)btnNum, (int)timeHolding);
}

