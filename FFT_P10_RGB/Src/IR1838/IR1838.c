/*
 * IrDecode.c
 *
 *  Created on: Feb 23, 2019
 *      Author: chungnguyen
 */
#include "myHeader.h"

#define ERR_CODE 0xFF
#define NUM_OF_BUTTON 21

typedef enum
{
	TIME_START_BIT_MIN = 4000,
	TIME_START_BIT_MAX = 6000,
	TIME_BIT_0_MIN = 1000,
	TIME_BIT_0_MAX = 1500,
	TIME_BIT_1_MIN = 2000,
	TIME_BIT_1_MAX = 2500,
	TIME_STOP_BIT_MIN = 40000,
	TIME_STOP_BIT_MAX = 50000,
} timeOfBit_Type_t;

typedef enum
{
	START = 1,
	DATA = 18,
	STOP = 34,
} posOfBit_t;

static void funNum(uint8_t num);

static Ir_Type_t Ir_Type;
static TIM_HandleTypeDef *TIM_IC = &htim2;
static struct keyboard_t
{
	uint8_t id;
	uint8_t value;
	void (*func)(uint8_t);
} keyboard[NUM_OF_BUTTON] =
		{
				{0x45, 10, NULL},  // "POWER",
				{0x44, 11, NULL},  // "MODE",
				{0x07, 12, NULL},  // "BACK",
				{0x16, 13, NULL},  // "VOLD",
				{0x0C, 1, funNum}, // "NUM1",
				{0x08, 4, funNum}, // "NUM4",
				{0x42, 7, funNum}, // "NUM7",

				{0x46, 14, NULL},  // "STOP",
				{0x40, 15, NULL},  // "RETURN",
				{0x15, 16, NULL},  // "FORWARD",
				{0x19, 17, NULL},  // "VOLU",
				{0x18, 2, funNum}, // "NUM2",
				{0x1C, 5, funNum}, // "NUM5",
				{0x52, 8, funNum}, //"NUM8",

				{0x47, 18, NULL},  // "mute",
				{0x43, 19, NULL},  // "EQ",
				{0x09, 20, NULL},  // "END",
				{0x0D, 0, funNum}, // "NUM0",
				{0x5E, 3, funNum}, // "NUM3",
				{0x5A, 6, funNum}, // "NUM6",
				{0x4A, 9, funNum}, // "NUM9",
		};
static uint8_t getBitAt(uint8_t pos);
static uint8_t getCode(uint8_t posDat);
static void processNewFrame(void);

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM_IC->Instance)
	{
		IR1838_HandleEvent();
	}
}

void IR1838_Init(void)
{
	HAL_TIM_IC_Start_IT(TIM_IC, TIM_CHANNEL_1);
}

void IR1838_HandleEvent(void)
{
	Ir_Type.tempCount2 = __HAL_TIM_GET_COUNTER(TIM_IC);
	Ir_Type.value[Ir_Type.index] = Timer_elapsedtime(Ir_Type.tempCount2, Ir_Type.tempCount1);
	Ir_Type.tempCount1 = Ir_Type.tempCount2;
	Ir_Type.index++;
	Ir_Type.timeOut = 60;
}

void IR1838_Process(void)
{
	if(Ir_Type.timeOut > 0)
	{
		if(--Ir_Type.timeOut == 0)
		{
			for(uint8_t i = 0; i < Ir_Type.index; i++)
				printf("value[%d] = %d\r\n", i, (int)Ir_Type.value[i]);
			processNewFrame();
			Ir_Type.index = 0;
		}
	}
}

void processNewFrame(void)
{
	if(Ir_Type.value[START] > TIME_START_BIT_MIN && Ir_Type.value[START] < TIME_START_BIT_MAX)
	{
		if(Ir_Type.value[STOP] > TIME_STOP_BIT_MIN && Ir_Type.value[STOP] < TIME_STOP_BIT_MAX)
		{
			uint16_t data = getCode(DATA);
			printf("code = 0x%2X, time = %d\r\n", data, (int)g_Systime);
			for(uint8_t i = 0; i < NUM_OF_BUTTON; i++)
				if(data == keyboard[i].id)
				{
					if(keyboard[i].func) keyboard[i].func(keyboard[i].value);
				}
		}
	}
	for(uint8_t i = 0; i < 40; i++) Ir_Type.value[i] = 0;
}

// get data bit at pos, pos must be rather than 0
static uint8_t getBitAt(uint8_t pos)
{
	if(Ir_Type.value[pos] > TIME_BIT_0_MIN && Ir_Type.value[pos] < TIME_BIT_0_MAX) return 0;
	else if(Ir_Type.value[pos] > TIME_BIT_1_MIN && Ir_Type.value[pos] < TIME_BIT_1_MAX) return 1;
	return ERR_CODE;
}

static uint8_t getCode(uint8_t posDat)
{
	uint8_t data = 0x00, tem;
	for(uint8_t i = posDat; i < posDat+15; i++)
	{
		tem = getBitAt(i);
		if(tem != ERR_CODE)
			data |= tem << (i-posDat);
		else return ERR_CODE;
	}
	return data;
}

static void funNum(uint8_t num)
{
	printf("num: %d\n", num);
}
