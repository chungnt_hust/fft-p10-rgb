/*
 * timer_user.h
 *
 *  Created on: Aug 6, 2019
 *      Author: chungnguyen
 */

#ifndef TIMER_USER_TIMER_USER_H_
#define TIMER_USER_TIMER_USER_H_

#include "myHeader.h"

extern volatile uint32_t g_Systime;

void Timer_Init(void);
bool Timer_getFlag(void);
void Timer_clearFlag(void);
uint32_t Timer_elapsedtime(uint32_t newTime, uint32_t oldTime);
bool Timer_checkpasstime(uint32_t newTime, uint32_t oldTime);
#endif /* TIMER_USER_TIMER_USER_H_ */
