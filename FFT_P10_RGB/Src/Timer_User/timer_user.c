/*
 * timer_user.c
 *
 *  Created on: Aug 6, 2019
 *      Author: chungnguyen
 */
#include "myHeader.h"
static TIM_HandleTypeDef *TIMER_SYSTEM = &htim6;
static volatile bool timerIntFlag = false;
volatile uint32_t g_Systime = 0;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIMER_SYSTEM->Instance)
	{
		timerIntFlag = true;
		g_Systime++;
	}
	else
		FFT_TimerInteruptHandle(htim);
}

void Timer_Init(void)
{
	HAL_TIM_Base_Start_IT(TIMER_SYSTEM);
}

bool Timer_getFlag(void)
{
	return timerIntFlag;
}

void Timer_clearFlag(void)
{
	timerIntFlag = false;
}

uint32_t Timer_elapsedtime(uint32_t newTime, uint32_t oldTime)
{
	return (newTime - oldTime);
}

bool Timer_checkpasstime(uint32_t newTime, uint32_t oldTime)
{
	return ((Timer_elapsedtime(newTime, oldTime) < 100000) ? true : false);
}
