/*
 * adc_user.c
 *
 *  Created on: Mar 6, 2020
 *      Author: chungnguyen
 */
#include "adc_user.h"

ADC_HandleTypeDef *ADC_MODULE = &hadc1;
uint16_t vOffset;

uint16_t readOffset(void)
{
	HAL_ADC_Start(ADC_MODULE);
	while(!__HAL_ADC_GET_FLAG(ADC_MODULE, ADC_FLAG_EOC));
	HAL_ADC_Stop(ADC_MODULE);
	return (uint16_t)HAL_ADC_GetValue(ADC_MODULE);
}

/* READ SIGNAL */
int16_t ADC_User_read(void)
{
	HAL_ADC_Start(ADC_MODULE);
	while(!__HAL_ADC_GET_FLAG(ADC_MODULE, ADC_FLAG_EOC));
	HAL_ADC_Stop(ADC_MODULE);
	return (int16_t)(HAL_ADC_GetValue(ADC_MODULE) - vOffset);
}

void ADC_User_calib(void)
{
	uint8_t count;
	uint32_t sum = 0;
	for(count = 0; count < 200; count++)
	{
		sum += readOffset();
		HAL_Delay(1);
	}
	vOffset = sum/200;
}
