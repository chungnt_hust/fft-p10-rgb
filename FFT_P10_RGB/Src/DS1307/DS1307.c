/*
 * i2c_user.c
 *
 *  Created on: Feb 3, 2019
 *      Author: chungnguyen
 */
#include "myHeader.h"

#define DS1307_ADD 0x68
static I2C_HandleTypeDef *I2CBUS = &hi2c2;
static uint8_t DateTime[7];

#define vSec   DateTime[SECONDS]
#define vMin   DateTime[MINUTES]
#define vHour  DateTime[HOURS]
#define vDay   DateTime[DAY]
#define vDate  DateTime[DATE]
#define vMonth DateTime[MONTH]
#define vYear  DateTime[YEAR]

static uint8_t DEC_BCD(uint8_t DEC)
{
	uint8_t temp = DEC;
	return (((DEC/10)*16) + (temp%10));
}
/*---------------*/
static uint8_t BCD_DEC(uint8_t BCD)
{
	uint8_t temp = BCD;
	return ((BCD/16)*10 + temp%16);
}

void DS1307_Init(void)
{
	DS1307_ReadMulti(SECONDS, DateTime, 7);
}

void DS1307_WriteMulti(registor_t add, uint8_t *txData, uint16_t size)
{
	for(uint8_t i = 0; i < size; i++) txData[i] = DEC_BCD(txData[i]);
	HAL_I2C_Mem_Write(I2CBUS, DS1307_ADD<<1, add, 1, txData, size, 100);
}

void DS1307_ReadMulti(registor_t add, uint8_t *rxData, uint16_t size)
{
	HAL_I2C_Mem_Read(I2CBUS, DS1307_ADD<<1, add, 1, rxData, size, 100);
	for(uint8_t i = 0; i < size; i++) rxData[i] = BCD_DEC(rxData[i]);
}

void DS1307_WriteByte(registor_t add, uint8_t *dataW)
{
	DS1307_WriteMulti(add, dataW, 1);
}

void DS1307_ReadByte(registor_t add, uint8_t *dataR)
{
	DS1307_ReadMulti(add, dataR, 1);
}

void DS1307_GetTime(void)
{
	static uint8_t secondOld = 1;
	DS1307_ReadByte(SECONDS, &vSec);
	if(vSec != secondOld)
	{
	  secondOld = vSec;
	  if(vSec == 0)
	  {
		  DS1307_ReadByte(MINUTES, &vMin);
		  if(vMin == 0)
		  {
			  DS1307_ReadByte(HOURS, &vHour);
		  }
	  }
	  printf("hms: %2d %2d %2d\n", vHour, vMin, vSec);
	}
	else printf("X\n");
}
