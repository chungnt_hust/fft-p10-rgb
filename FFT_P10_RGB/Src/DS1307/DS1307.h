/*
 * i2c_user.h
 *
 *  Created on: Feb 3, 2019
 *      Author: chungnguyen
 */

#ifndef DS1307_H_
#define DS1307_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>

typedef enum
{
	BASE = 0x00,
	SECONDS = 0x00,
	MINUTES,
	HOURS,
	DAY,
	DATE,
	MONTH,
	YEAR,
} registor_t;

void DS1307_Init(void);
void DS1307_WriteMulti(registor_t add, uint8_t *txData, uint16_t size);
void DS1307_ReadMulti(registor_t add, uint8_t *rxData, uint16_t size);
void DS1307_WriteByte(registor_t add, uint8_t *dataW);
void DS1307_ReadByte(registor_t add, uint8_t *dataR);
void DS1307_GetTime(void);
#endif /* DS1307_H_ */
