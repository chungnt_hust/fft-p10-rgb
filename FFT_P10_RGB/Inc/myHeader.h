/*
 * myHeader.h
 *
 *  Created on: Aug 6, 2019
 *      Author: chungnguyen
 */

#ifndef MYHEADER_H_
#define MYHEADER_H_

#include "stm32f3xx_hal.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "usart.h"
#include "uart_user.h"
#include "tim.h"
#include "timer_user.h"
#include "button.h"
#include "IR1838.h"
#include "i2c.h"
#include "DS1307.h"
#include "adc_user.h"
#include "FFT.h"
#endif /* MYHEADER_H_ */
