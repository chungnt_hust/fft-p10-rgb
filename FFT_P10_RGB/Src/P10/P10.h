/*
 * P10.h
 *
 *  Created on: Jan 24, 2019
 *      Author: chungnguyen
 */

#ifndef P10_P10_H_
#define P10_P10_H_
#include "myHeader.h"
#define MAX_ROW_SCAN 8
#define MAX_ROW		 16
#define MAX_COL		 32
#define MAX_BIT_POS  8
typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} pixel_t;

typedef struct
{
	uint8_t x;
	uint8_t y;
} point_2D_t;

typedef struct
{
	uint8_t encodeData[MAX_ROW_SCAN][MAX_BIT_POS]; // MAX_ROW_SCAN: sÃ¡Â»â€˜ hÃƒÂ ng quÃƒÂ©t, MAX_BIT_POS: sÃ¡Â»â€˜ bit sÃƒÂ¡ng cho mÃ¡Â»â€”i led
} Encode_t;

typedef enum pinNumberP10
{
	R1_PIN = 0,
	G1_PIN,
	B1_PIN,
	R2_PIN,
	G2_PIN,
	B2_PIN,
	CLK_PIN,
	A_PIN,
	B_PIN,
	C_PIN,
	LAT_PIN,
	OE_PIN,
	MAX_NUM_PIN_P10
} pinNumber_t;

void P10_setColorAt(uint8_t row, uint8_t col, uint8_t r, uint8_t g, uint8_t b);
void P10_display(void);
void P10_enableDisplay(void);
void P10_clearScreen(void);

#endif /* P10_P10_H_ */
