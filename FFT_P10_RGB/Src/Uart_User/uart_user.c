/*
 * uart_user.c
 *
 *  Created on: Jan 30, 2019
 *      Author: chungnguyen
 */
#include "uart_user.h"

static UART_HandleTypeDef *UART = &huart2;
static buffer_t bufferRx;

static void getNewByte(void)
{
	bufferRx.Data[bufferRx.Index] = bufferRx.Byte;
	bufferRx.Index++;
	bufferRx.State = 3;
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == UART->Instance)
	{
		getNewByte();
	}
}

void Uart_Init(void)
{
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
	printf("Init done!\r\n");
}

void Uart_Process(void)
{
	if(bufferRx.State > 0)
	{
		bufferRx.State--;
		if(bufferRx.State == 0)
		{
			Uart_ProcessNewData();
		}
	}
}

void Uart_ProcessNewData(void)
{
	bufferRx.Data[bufferRx.Index] = 0;
	printf("%s\r\n", bufferRx.Data);
	if(bufferRx.Data[0] == '[' && bufferRx.Data[bufferRx.Index-1] == ']')
	{
		// do something
	}
	else printf("command not support\r\n");

	bufferRx.Index = 0;
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
}
