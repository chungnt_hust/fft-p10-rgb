/*
 * P10.c
 *
 *  Created on: Jan 24, 2019
 *      Author: chungnguyen
 */
#include "P10.h"
#include "font.h"
/*
 *  HCLK = 72MHz
 	SysTick->LOAD = 72000;
	SysTick->VAL  = 0;
	<=> 1ms
*/
#define TIME_INT (500) // 7us
static GPIO_TypeDef *gpioPort[]  = {GPIOB,          GPIOB, 		    GPIOB, 		 	GPIOB, 			GPIOB, 		    GPIOB, 		    GPIOB, 	        GPIOA, 		    GPIOA, 		    GPIOA, 		    GPIOA, 		    GPIOA};
static uint32_t valueGpioSet[]   = {GPIO_PIN_0,     GPIO_PIN_1,     GPIO_PIN_2,     GPIO_PIN_3,     GPIO_PIN_4,     GPIO_PIN_5,     GPIO_PIN_6,     GPIO_PIN_4,     GPIO_PIN_6,     GPIO_PIN_7,     GPIO_PIN_8,     GPIO_PIN_11};
static uint32_t valueGpioReSet[] = {GPIO_PIN_0<<16, GPIO_PIN_1<<16, GPIO_PIN_2<<16, GPIO_PIN_3<<16, GPIO_PIN_4<<16, GPIO_PIN_5<<16, GPIO_PIN_6<<16, GPIO_PIN_4<<16, GPIO_PIN_6<<16, GPIO_PIN_7<<16, GPIO_PIN_8<<16, GPIO_PIN_11<<16};
static GPIO_TypeDef *gpioDataPort = GPIOB;
static pixel_t Panel[MAX_ROW][MAX_COL]; // dá»¯ liá»‡u = 1 <=> led sÃ¡ng
static uint32_t timeSlice[MAX_BIT_POS] = {1*TIME_INT-1, 2*TIME_INT-1, 4*TIME_INT-1, 8*TIME_INT-1, 16*TIME_INT-1, 32*TIME_INT-1, 64*TIME_INT-1, 128*TIME_INT-1};
static point_2D_t P10;
static bool enDpl;

Encode_t DataOutput[MAX_COL];

static inline void switchRowP10(uint8_t row)
{
	(row & 0x1) ? (gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioSet[A_PIN]):(gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioReSet[A_PIN]);
	(row & 0x2) ? (gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioSet[B_PIN]):(gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioReSet[B_PIN]);
	(row & 0x4) ? (gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioSet[C_PIN]):(gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioReSet[C_PIN]);
//	gpioPort[A_PIN]->ODR = ((gpioPort[A_PIN]->ODR)&0xf8) | row;
}

static inline void shiftData(uint8_t row, uint8_t pos)
{
	uint8_t x;
	for(x = 0; x < MAX_COL; x++)
	{
		gpioDataPort->ODR = ((uint32_t)DataOutput[x].encodeData[row][pos]);
		gpioPort[CLK_PIN]->BSRR = valueGpioSet[CLK_PIN];
	}
}

/* system tick interrupt callback */
void HAL_SYSTICK_Callback(void)
{
	static uint8_t row;
	static uint8_t bitPos;
	gpioPort[OE_PIN]->BSRR = valueGpioSet[OE_PIN];
	shiftData(row, bitPos);
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioReSet[LAT_PIN];
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioSet[LAT_PIN];
	switchRowP10(row);
	gpioPort[OE_PIN]->BSRR = valueGpioReSet[OE_PIN];

	SysTick->LOAD = timeSlice[bitPos];
	SysTick->VAL  = 0;

	bitPos++;
	if(bitPos == MAX_BIT_POS)
	{
		bitPos = 0;
		row++;
		if(row == MAX_ROW_SCAN) row = 0;
	}
}

/* 0 <= row <= 15; 0 <= col <= 64 */
static void P10_position(uint8_t row, uint8_t col)
{
	P10.x = col;
	P10.y = row;
}

/* 0 <= r, g, b <= 255*/
static void P10_color(uint8_t r, uint8_t g, uint8_t b)
{
	Panel[P10.y][P10.x].r = r;
	Panel[P10.y][P10.x].g = g;
	Panel[P10.y][P10.x].b = b;
}

void P10_setColorAt(uint8_t row, uint8_t col, uint8_t r, uint8_t g, uint8_t b)
{
	P10_position(row, col);
	P10_color(r, g, b);
}

void P10_display(void)
{
	if(enDpl)
	{
		uint8_t row, col, pos;
		for(col = 0; col < MAX_COL; col++)
		{
			for(row = 0; row < MAX_ROW_SCAN; row++)
			{
				uint8_t temRow = row+8;
				for(pos = 0; pos < MAX_BIT_POS; pos++)
				{
					DataOutput[col].encodeData[row][pos] = ((Panel[row][col].r>>pos & 0x1)<<0) |
														   ((Panel[row][col].g>>pos & 0x1)<<1) |
														   ((Panel[row][col].b>>pos & 0x1)<<2) |
														   ((Panel[temRow][col].r>>pos & 0x1)<<3) |
														   ((Panel[temRow][col].g>>pos & 0x1)<<4) |
														   ((Panel[temRow][col].b>>pos & 0x1)<<5);
				}
			}
		}
		enDpl = false;
	}
}

void P10_enableDisplay(void)
{
	enDpl = true;
}

void P10_clearScreen(void)
{
	for(uint8_t row = 0; row < MAX_ROW; row++)
	{
		for(uint8_t col = 0; col < MAX_COL; col++)
		{
			Panel[row][col].r = Panel[row][col].g = Panel[row][col].b = 0;
		}
	}
}

#ifdef TEST_FONT
void P10_testFont(void)
{
	uint8_t row, col;
	uint16_t ii;
	for(row = 0; row < 16; row++)
	{
		for(col = 0; col < 32; col++)
		{
			ii = 32*row + col;
			P10_setColorAt(row, col, testFont[ii][0], testFont[ii][1], testFont[ii][2]);
		}
	}
	P10_enableDisplay();
	P10_display();
}
#endif
